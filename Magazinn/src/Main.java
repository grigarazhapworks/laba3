import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        int b = 10;
        int c = 12;
        int i = 0;
        int i1 = 0;
        int k = 0;
        VideoCards[] videoCard = new VideoCards[b];
        Monitors[] monitor = new Monitors[c];
        Scanner scan = new Scanner(System.in);
        while (true){
            System.out.println("1 - Добавление товара \n2 - Редактирование товара \n3 - Просмотр товара");
            String input = scan.next();

            switch (input){
                case "1":
                    System.out.println("Добавление товара");
                    System.out.println("Выберите тип товара. 1 - видеокарты, 2 - мониторы");
                    k = scan.nextInt();
                    switch(k) {
                        case 1:
                            videoCard[i++] = new VideoCards(typeValue("Введите производителя"), typeValue("Введите название"), Double.valueOf(typeValue("Введите цену")), Integer.valueOf(typeValue("Введите объем видеопамяти")), Float.valueOf(typeValue("Введите частоту")));
                            break;
                        case 2:
                            monitor[i1++]=new Monitors(typeValue("Введите производителя"), typeValue("Введите наименование"), Double.valueOf(typeValue("Введите цену")), Double.valueOf(typeValue("Введите диагональ в дюймах")), Float.valueOf(typeValue("Введите частоту")),typeValue("Введите тип матрицы"),new int[] {Integer.valueOf(typeValue("Введите ширину")),Integer.valueOf(typeValue("Введите высоту"))});

                    }
                    break;
                case "2":
                    System.out.println("Редактирование товара");
                    System.out.println("Выберите тип товара. 1 - видеокарты, 2 - мониторы");
                    k = scan.nextInt();
                    switch(k) {
                        case 1:
                            int id = scan.nextInt();
                            if (id <= i1) {
                                System.out.println(videoCard[id]);
                                System.out.println("Выберите, что редактировать:\n1 - производитель\n2 - название\n3 - цена \n 4 - видеопамять\n5 - частота\n6 - все\n= - назад");
                                String vib = scan.next();
                                while (vib!="=") {
                                    switch (vib) {
                                        case "1":
                                            videoCard[id].setProizvod(typeValue("Введите нового произодителя"));
                                            break;
                                        case "2":
                                            videoCard[id].setName(typeValue("Введите новое название"));
                                            break;
                                        case "3":
                                            videoCard[id].setCost(Double.valueOf(typeValue("Введите новую цену")));
                                            break;
                                        case "4":
                                            videoCard[id].setVideoMemory(Integer.valueOf(typeValue("Введите новую видеопамять")));
                                            break;
                                        case "5":
                                            videoCard[id].setFrequency(Float.valueOf(typeValue("Введите новую частоту")));
                                            break;
                                        case "6":
                                            videoCard[id] = new VideoCards(typeValue("Введите производителя"), typeValue("Введите название"), Double.valueOf(typeValue("Введите цену")), Integer.valueOf(typeValue("Введите объем видеопамяти")), Float.valueOf(typeValue("Введите частоту")));
                                            break;
                                        default:
                                            break;
                                    }
                                    vib = scan.next();
                                }
                            }
                            break;
                        case 2:
                            int id1 = scan.nextInt();
                            if (id1 <= i) {
                                System.out.println(monitor[id1]);
                                System.out.println("Выберите, что редактировать:\n1 - производитель\n2 - название\n3 - цена \n 4 - диагональ\n5 - частота\n6 - разрешение экрана\n7 - тип матрицы\n8 - все\n= - назад");
                                String vib = scan.next();
                                while (!vib.equals("=")) {
                                    vib = scan.next();
                                    switch (vib) {
                                        case "1":
                                            monitor[id1].setProizvod(typeValue("Введите нового произодителя"));
                                            break;
                                        case "2":
                                            monitor[id1].setName(typeValue("Введите новое название"));
                                            break;
                                        case "3":
                                            monitor[id1].setCost(Double.valueOf(typeValue("Введите новую цену")));
                                            break;
                                        case "4":
                                            monitor[id1].setDiag(Double.valueOf(typeValue("Введите новую диагональ")));
                                            break;
                                        case "5":
                                            monitor[id1].setFrequency(Float.valueOf(typeValue("Введите новую частоту")));
                                            break;
                                        case "6":
                                            monitor[id1].setResolution(new int[] {Integer.valueOf(typeValue("Введите новую ширину")),Integer.valueOf(typeValue("Введите новую высоту"))});
                                        case "7":
                                            monitor[id1].setType(typeValue("Введите новый тип матрицы"));
                                        case "8":
                                            monitor[id1] = new Monitors(typeValue("Введите нового производителя"), typeValue("Введите новое наименование"), Double.valueOf(typeValue("Введите новую цену")), Double.valueOf(typeValue("Введите новую диагональ в дюймах")), Float.valueOf(typeValue("Введите новую частоту")),typeValue("Введите новый тип матрицы"),new int[] {Integer.valueOf(typeValue("Введите новую ширину")),Integer.valueOf(typeValue("Введите новую высоту"))});
                                        default:
                                            break;
                                    }
                                    vib = scan.next();
                                }
                            }
                    }
                    break;
                case "3":
                    System.out.println("Просмотр товара");
                    System.out.println("Выберите тип товара. 1 - Видеокарты. 2 - Мониторы.");
                    k = scan.nextInt();
                    switch(k) {
                        case 1:
                            for (int j = 0; j < i; j++) System.out.println(videoCard[j]);
                            break;
                        case 2:
                            for(int j = 0;j<i1;j++) System.out.println(monitor[j]);
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

    }
    public static String typeValue(String type){
        Scanner scan = new Scanner(System.in);
        System.out.println(type);
        return scan.next();
    }
}
