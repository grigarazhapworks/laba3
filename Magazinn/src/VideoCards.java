public class VideoCards {
    private String proizvod;
    private int videoMemory;
    private float frequency;
    private String name;
    private double cost;

    public VideoCards(String proizvod, String name, double cost, int videoMemory, float frequency){
        this.proizvod = proizvod;
        this.name = name;
        this.cost = cost;
        this.videoMemory = videoMemory;
        this.frequency = frequency;
    }

    public void setProizvod(String proizvod) {
        this.proizvod = proizvod;
    }

    public void setVideoMemory(int videoMemory) {
        this.videoMemory = videoMemory;
    }

    public void setFrequency(float frequency) {
        this.frequency = frequency;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getProizvod() {
        return proizvod;
    }

    public int getVideoMemory() {
        return videoMemory;
    }

    public float getFrequency() {
        return frequency;
    }

    public String getName() {
        return name;
    }

    public double getCost() {
        return cost;
    }

    @Override
    public String toString() {
        return String.format("Производитель: %s\n Название: %s \n " +
                "Цена: %f \n Видеопамять: %d \n Частота: %f", proizvod, name, cost, videoMemory, frequency);
    }
}