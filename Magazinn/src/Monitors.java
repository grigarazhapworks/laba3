public class Monitors
{
    private String proizvod;
    private double diag;
    private float frequency;
    private String name;
    private String type;
    private int[] resolution;
    private double cost;

    public Monitors(String proizvod, String name, double cost, double diag, float frequency,String type,int[] resolution){
        this.proizvod = proizvod;
        this.name = name;
        this.cost = cost;
        this.diag = diag;
        this.frequency = frequency;
        this.type = type;
        this.resolution = resolution;
    }

    public void setProizvod(String proizvod) {
        this.proizvod = proizvod;
    }

    public void setDiag(double diag) {
        this.diag = diag;
    }
    public void setResolution(int[] resolution){
        this.resolution = resolution;
    }

    public void setFrequency(float frequency) {
        this.frequency = frequency;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
    public String setType(String type){
        return type;
    }

    public String getProizvod() {
        return proizvod;
    }

    public double getDiag() {
        return diag;
    }

    public float getFrequency() {
        return frequency;
    }

    public String getName() {
        return name;
    }

    public double getCost() {
        return cost;
    }
    public String getType(){
        return type;
    }
    public int[] getResolution(){
        return resolution;
    }

    @Override
    public String toString() {
        return String.format("Производитель: %s\n Название: %s \n " +
                "Цена: %f \n Диагональ: %f дюймов\n Частота: %f Гц\n Разрешение: %d x %d\n Тип матрицы: %s" , proizvod, name, cost, diag, frequency,resolution[0],resolution[1],type);
    }
}
